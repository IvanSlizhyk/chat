﻿namespace Chat.Core.Entities
{
    public class UserSetting : BaseEntity<int>
    {
        public Setting Setting { get; set; }
        public int SettingId { get; set; }

        public SettingValue SettingValue { get; set; }
        public int SettingValueId { get; set; }

        public User User { get; set; }
        public int UserId { get; set; }
    }
}
