﻿using System;
using System.Collections.Generic;

namespace Chat.Core.Entities
{
    public class User : BaseEntity<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<User> Friends { get; set; }
        public List<Message> Messages { get; set; }

        //public UserSetting UserSetting { get; set; }
    }
}
