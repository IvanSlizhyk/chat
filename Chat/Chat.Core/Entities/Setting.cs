﻿using System.Collections.Generic;

namespace Chat.Core.Entities
{
    public class Setting : BaseEntity<int>
    {
        public string Name { get; set; }
        public List<SettingValue> SettingValue { get; set; }
        public ComponentType ComponentType { get; set; }  
    }
}
