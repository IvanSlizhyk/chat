﻿namespace Chat.Core.Entities
{
    public enum ComponentType
    {
        CheckBox = 0,
        ComboBox = 1,
        RadioButton = 2,
        TextBox = 3
    }
}
