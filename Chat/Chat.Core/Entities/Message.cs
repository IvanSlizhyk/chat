﻿using System;

namespace Chat.Core.Entities
{
    public class Message : BaseEntity<int>
    {
        public User Sender { get; set; }
        public int SenderId { get; set; }

        public DateTime CreatedDate { get; set; }
        public string Content { get; set; }

        public User Receiver { get; set; }
        public int ReceiverId { get; set; }
    }
}
