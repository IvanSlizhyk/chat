﻿namespace Chat.Core.Entities
{
    public class SettingValue : BaseEntity<int>
    {
        public string Value { get; set; }
        public string Type { get; set; }
    }
}
