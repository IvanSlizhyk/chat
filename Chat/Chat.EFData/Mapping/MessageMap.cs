﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chat.Core.Entities;

namespace Chat.EFData.Mapping
{
    public class MessageMap : EntityTypeConfiguration<Message>
    {
        public MessageMap()
        {
            HasKey(h => h.Id);
            Property(h => h.Content).IsRequired().HasMaxLength(1000);
            Property(h => h.CreatedDate).IsRequired();
            HasRequired(h => h.Sender).WithMany(h => h.Messages).HasForeignKey(h => h.SenderId);
            HasRequired(h => h.Receiver).WithMany(h => h.Messages).HasForeignKey(h => h.ReceiverId);
        }
    }
}
