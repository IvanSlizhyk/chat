﻿using System.Data.Entity.ModelConfiguration;
using Chat.Core.Entities;

namespace Chat.EFData.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            HasKey(h => h.Id);
            Property(h => h.FirstName).IsRequired().HasMaxLength(20);
            Property(h => h.LastName).IsRequired().HasMaxLength(20);
            Property(h => h.CreatedDate).IsRequired();
            HasMany(h => h.Messages).WithRequired(h => h.Sender).HasForeignKey(h => h.SenderId);
            HasOptional(h => h.Friends).WithMany().HasForeignKey(h => h.Id);
        }
    }
}