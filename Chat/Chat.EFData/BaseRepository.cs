﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat.EFData
{
    public class BaseRepository
    {
        protected ChatContext Context { get; set; }

        public BaseRepository(ChatContext context)
        {
            Context = context;
        }
    }
}
