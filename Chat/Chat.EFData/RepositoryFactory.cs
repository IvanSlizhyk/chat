﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chat.Core.Entities;
using Chat.DALInterfaces;

namespace Chat.EFData
{
    public class RepositoryFactory : IRepositoryFactory
    {
        private readonly ChatContext _context;
        private IRepositoryGeneric<User, int> _userRepository;
        private IRepositoryGeneric<Message, int> _messageRepository;

        public RepositoryFactory(ChatContext context)
        {
            _context = context;
        }

        public IRepositoryGeneric<User, int> GetUserRepository()
        {
            return _userRepository ?? (_userRepository = new RepositoryGeneric<User, int>(_context));
        }

        public IRepositoryGeneric<Message, int> GetMessageRepository()
        {
            return _messageRepository ?? (_messageRepository = new RepositoryGeneric<Message, int>(_context));
        }
    }
}