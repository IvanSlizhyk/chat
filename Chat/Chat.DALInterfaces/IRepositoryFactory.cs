﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chat.Core.Entities;

namespace Chat.DALInterfaces
{
    public interface IRepositoryFactory
    {
        IRepositoryGeneric<User, int> GetUserRepository();
        IRepositoryGeneric<Message, int> GetMessageRepository();
    }
}
