﻿using System;
using System.Collections.Generic;
using Chat.BLLInterfaces.BLInterfaces;
using Chat.BLLInterfaces.Exceptions;
using Chat.Core.Entities;
using Chat.DALInterfaces;

namespace Chat.Services.Services
{
    public class MessageService : BaseService, IMessageService
    {
        public MessageService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {
        }

        public Message CreateMessage(string content, DateTime createdDate, User sender, User receiver)
        {
            var messageRepository = RepositoryFactory.GetMessageRepository();

            var senderMessage = new Message
            {
                Content = content,
                CreatedDate = createdDate
            };
            var receiverMessage = new Message
            {
                Content = content,
                CreatedDate = createdDate
            };

            messageRepository.Create(senderMessage);
            messageRepository.Create(receiverMessage);

            try
            {
                UnitOfWork.Save();
            }
            catch (MessageServiceException e)
            {
                throw new MessageServiceException(e);
            }

            SetSenderAndReceiverToMessage(senderMessage, sender, receiver);
            SetSenderAndReceiverToMessage(receiverMessage, receiver, sender);

            return senderMessage;
        }

        public void SetSenderAndReceiverToMessage(Message message, User sender, User receiver)
        {
            message.Sender = sender;
            message.SenderId = sender.Id;
            message.Receiver = receiver;
            message.ReceiverId = receiver.Id;
        }

        public void RemoveMessage(Message message)
        {
            var messageRepository = RepositoryFactory.GetMessageRepository();

            try
            {
                messageRepository.Remove(message);
            }
            catch (MessageServiceException e)
            {
                throw new MessageServiceException(e);
            }
        }

        public void RemoveAllMessagesBetweenUserAndFriend(User user, User friend)
        {
            try
            {
                user.Messages.RemoveAll(h => h.ReceiverId == friend.Id || h.SenderId == friend.Id);
            }
            catch (MessageServiceException e)
            {
                throw new MessageServiceException(e);
            }
        }

        public List<Message> GetAllMessagesBetweenUserAndFriend(User user, User friend)
        {
            try
            {
                var messages = user.Messages.FindAll(h => h.ReceiverId == friend.Id || h.SenderId == friend.Id);
                return messages;
            }
            catch (MessageServiceException e)
            {
                throw new MessageServiceException(e);
            }
        }
    }
}
