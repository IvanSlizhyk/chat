﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chat.BLLInterfaces.BLInterfaces;
using Chat.DALInterfaces;

namespace Chat.Services.Services
{
    public class BaseService : IService
    {
        protected IUnitOfWork UnitOfWork { get; set; }
        protected IRepositoryFactory RepositoryFactory { get; set; }

        public BaseService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
        {
            UnitOfWork = unitOfWork;
            RepositoryFactory = repositoryFactory;
        }
    }
}