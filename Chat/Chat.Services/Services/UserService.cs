﻿using System;
using System.Collections.Generic;
using System.Linq;
using Chat.BLLInterfaces.BLInterfaces;
using Chat.BLLInterfaces.Exceptions;
using Chat.Core.Entities;
using Chat.DALInterfaces;

namespace Chat.Services.Services
{
    public class UserService : BaseService, IUserService
    {
        public UserService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {
        }

        public User CreateUser(string firstName, string lastName, DateTime createdDate)
        {
            var userRepository = RepositoryFactory.GetUserRepository();
            var user = new User
            {
                FirstName = firstName,
                LastName = lastName,
                CreatedDate = createdDate
            };

            userRepository.Create(user);

            try
            {
                UnitOfWork.Save();
            }
            catch (UserServiceException e)
            {
                throw new UserServiceException(e);
            }

            return user;
        }

        public User GetUserById(int userId)
        {
            var userRepository = RepositoryFactory.GetUserRepository();

            try
            {
                var user = userRepository.GetEntityById(userId);
                return user;
            }
            catch (UserServiceException e)
            {
                throw new UserServiceException(e);
            }
        }

        public List<User> GetAllFriendsByName(string name, int userId)
        {
            var userRepository = RepositoryFactory.GetUserRepository();
            var user = userRepository.GetEntityById(userId);

            try
            {
                var friends = GetUsersByName(name, user.Friends);
                return friends;
            }
            catch (UserServiceException e)
            {
                throw new UserServiceException(e);
            }
        }

        public List<User> GetAllUsersByName(string name)
        {
            var userRepository = RepositoryFactory.GetUserRepository();
            var allUsers = userRepository.All();

            try
            {
                var users = GetUsersByName(name, allUsers.ToList());
                return users;
            }
            catch (UserServiceException e)
            {
                throw new UserServiceException(e);
            }
        }

        public List<User> GetUsersByName(string name, List<User> users)
        {
            return users.FindAll(h => h.FirstName.Contains(name) || h.LastName.Contains(name) || string.Format("{0} {1}", h.FirstName, h.LastName).Contains(name));
        }

        public void AddFriendToUser(User friend, int userId)
        {
            var userRepository = RepositoryFactory.GetUserRepository();
            var user = userRepository.GetEntityById(userId);
            user.Friends.Add(friend);
        }

        public void DeleteFriendFromUser(User friend, int userId)
        {
            var userRepository = RepositoryFactory.GetUserRepository();
            var user = userRepository.GetEntityById(userId);
            user.Friends.Remove(friend);
        }

        public void UpdateUser(User user)
        {
            var userRepository = RepositoryFactory.GetUserRepository();

            try
            {
                userRepository.Update(user);
            }
            catch (UserServiceException e)
            {
                throw new UserServiceException(e);
            }
        }

        public IQueryable<User> GetAllUsers()
        {
            var userRepository = RepositoryFactory.GetUserRepository();

            try
            {
                var users = userRepository.All();
                return users;
            }
            catch (UserServiceException e)
            {
                throw new UserServiceException(e);
            }
        }
    }
}