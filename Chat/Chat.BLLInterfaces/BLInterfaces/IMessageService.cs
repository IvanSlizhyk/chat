﻿using System;
using System.Collections.Generic;
using System.Linq;
using Chat.Core.Entities;

namespace Chat.BLLInterfaces.BLInterfaces
{
    public interface IMessageService : IService
    {
        Message CreateMessage(string content, DateTime createdDate, User sender, User receiver);
        void SetSenderAndReceiverToMessage(Message message, User sender, User receiver);
        void RemoveMessage(Message message);
        void RemoveAllMessagesBetweenUserAndFriend(User user, User friend);
        List<Message> GetAllMessagesBetweenUserAndFriend(User user, User friend);
    }
}