﻿using System;
using System.Collections.Generic;
using System.Linq;
using Chat.Core.Entities;

namespace Chat.BLLInterfaces.BLInterfaces
{
    public interface IUserService : IService
    {
        User CreateUser(string firstName, string lastName, DateTime createdDate);
        User GetUserById(int userId);
        List<User> GetAllFriendsByName(string name, int userId);
        List<User> GetAllUsersByName(string name);
        List<User> GetUsersByName(string name, List<User> users);
        void AddFriendToUser(User friend, int userId);
        void DeleteFriendFromUser(User friend, int userId);
        void UpdateUser(User user);
        IQueryable<User> GetAllUsers();
    }
}