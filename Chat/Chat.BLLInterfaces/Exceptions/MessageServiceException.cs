﻿using System;

namespace Chat.BLLInterfaces.Exceptions
{
    public class MessageServiceException : Exception
    {
        public MessageServiceException()
        {

        }

        public MessageServiceException(string message)
            : base(message)
        {

        }

        public MessageServiceException(Exception exception)
            : base("Some exception occured", exception)
        {

        }
    }
}