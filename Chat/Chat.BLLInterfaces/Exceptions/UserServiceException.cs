﻿using System;

namespace Chat.BLLInterfaces.Exceptions
{
    public class UserServiceException : Exception
    {
        public UserServiceException()
        {

        }

        public UserServiceException(string message)
            : base(message)
        {

        }

        public UserServiceException(Exception exception)
            : base("Some exception occured!", exception)
        {

        }
    }
}
